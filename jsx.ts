import type * as Hast from "hast";

type Override<T, U> = Omit<T, keyof U> & U;
type Expand<T> = T extends infer O ? { [K in keyof O]: O[K] } : never;
type PartialOrUndefined<T extends Record<string, unknown>> = {
  [K in keyof T]?: T[K] | undefined;
};

declare global {
  interface HTMLElementTagNameMap {
    meta: HTMLMetaElement;
  }

  type FormEncType =
    | "multipart/form-data"
    | "text/plain"
    | "application/x-www-urlencoded";
  type FormMethod = "get" | "post" | "dialog";

  namespace JSX {
    type AttributeDefinition = Record<string, string | number | boolean>;
    type EmptyAttributeDefinition = Record<never, string | number | boolean>;

    type ElementDefinition<
      Attributes extends JSX.AttributeDefinition = EmptyAttributeDefinition,
    > = Expand<PartialOrUndefined<Override<GlobalAttributes, Attributes>>>;

    type ElementDefinitionWithChildren<
      Attributes extends JSX.AttributeDefinition = EmptyAttributeDefinition,
    > = ElementDefinition<Attributes & { children: JSX.Node }>;

    interface GlobalAttributes {
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/accesskey)
       */
      accesskey: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/autocapitalize)
       */
      autocapitalize: "none" | "off" | "sentences" | "words" | "characters";
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/autofocus)
       */
      autofocus: boolean;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/class)
       */
      class: string | Record<string, boolean>;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/contenteditable)
       */
      contenteditable: boolean;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/data-*)
       */
      [data: `data-${string}`]: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/dir)
       */
      dir: "ltr" | "rtl" | "auto";
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/draggable)
       */
      draggable: boolean;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/enterkeyhint)
       */
      enterkeyhint:
        | "enter"
        | "done"
        | "go"
        | "next"
        | "previous"
        | "search"
        | "send";
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/exportparts)
       */
      exportparts: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/hidden)
       */
      hidden: boolean | "" | "hidden" | "until-found";
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/id)
       */
      id: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/inert)
       */
      inert: boolean;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/inputmode)
       */
      inputmode:
        | "none"
        | "text"
        | "decimal"
        | "numeric"
        | "tel"
        | "search"
        | "email"
        | "url";
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/is)
       */
      is: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/itemid)
       */
      itemid: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/itemprop)
       */
      itemprop: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/itemref)
       */
      itemref: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/itemscope)
       */
      itemscope: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/itemtype)
       */
      itemtype: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/lang)
       */
      lang: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/nonce)
       */
      nonce: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/part)
       */
      part: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/popover)
       */
      popover: boolean | "auto" | "manual";
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Roles)
       */
      role: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/slot)
       */
      slot: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/spellcheck)
       */
      spellcheck: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/style)
       */
      style: string | Partial<CSSStyleDeclaration>;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/tabindex)
       */
      tabindex: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/title)
       */
      title: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/translate)
       */
      translate: string;
      /**
       * [MDN Reference](/en-US/docs/Web/HTML/Global_attributes/virtualkeyboardpolicy)
       */
      virtualkeyboardpolicy: "auto" | "manual";
      Experimental: string;

      // event handlers

      onabort: string;
      onautocomplete: string;
      onautocompleteerror: string;
      onblur: string;
      oncancel: string;
      oncanplay: string;
      oncanplaythrough: string;
      onchange: string;
      onclick: string;
      onclose: string;
      oncontextmenu: string;
      oncuechange: string;
      ondblclick: string;
      ondrag: string;
      ondragend: string;
      ondragenter: string;
      ondragleave: string;
      ondragover: string;
      ondragstart: string;
      ondrop: string;
      ondurationchange: string;
      onemptied: string;
      onended: string;
      onerror: string;
      onfocus: string;
      oninput: string;
      oninvalid: string;
      onkeydown: string;
      onkeypress: string;
      onkeyup: string;
      onload: string;
      onloadeddata: string;
      onloadedmetadata: string;
      onloadstart: string;
      onmousedown: string;
      onmouseenter: string;
      onmouseleave: string;
      onmousemove: string;
      onmouseout: string;
      onmouseover: string;
      onmouseup: string;
      onmousewheel: string;
      onpause: string;
      onplay: string;
      onplaying: string;
      onprogress: string;
      onratechange: string;
      onreset: string;
      onresize: string;
      onscroll: string;
      onseeked: string;
      onseeking: string;
      onselect: string;
      onshow: string;
      onsort: string;
      onstalled: string;
      onsubmit: string;
      onsuspend: string;
      ontimeupdate: string;
      ontoggle: string;
      onvolumechange: string;
      onwaiting: string;

      // aria attributes

      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-atomic)
       */
      ariaAtomic: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-busy)
       */
      ariaBusy: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-controls)
       */
      ariaControls: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-current)
       */
      ariaCurrent: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby)
       */
      ariaDescribedby: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-description)
       */
      ariaDescription: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-details)
       */
      ariaDetails: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-disabled)
       */
      ariaDisabled: boolean;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-dropeffect)
       */
      ariaDropeffect: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-errormessage)
       */
      ariaErrormessage: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-flowto)
       */
      ariaFlowto: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-grabbed)
       */
      ariaGrabbed: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-haspopup)
       */
      ariaHaspopup: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-hidden)
       */
      ariaHidden: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-invalid)
       */
      ariaInvalid: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-keyshortcuts)
       */
      ariaKeyshortcuts: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-label)
       */
      ariaLabel: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-labelledby)
       */
      ariaLabelledby: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-live)
       */
      ariaLive: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-owns)
       */
      ariaOwns: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-relevant)
       */
      ariaRelevant: string;
      /**
       * [MDN Reference](/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-roledescription)
       */
      ariaRoledescription: string;
    }

    type Attributes<Element extends globalThis.Element = globalThis.Element> =
      Expand<
        Partial<
          Override<
            Element,
            JSX.GlobalAttributes &
              (Element extends { children: any }
                ? { children: JSX.Node }
                : {}) &
              (Element extends { form: any } ? { form: string } : {})
          >
        >
      >;

    namespace HTML {
      /**
       * the DOM HTMLElementTagNameMap is the JS side of things, this is the HTML side of those same things
       */
      interface ElementTagNameMap {
        // alphabetically sorted
        a: ElementDefinitionWithChildren<{
          download: string;
          href: string;
          hreflang: string;
          ping: string;
          referrerpolicy: ReferrerPolicy;
          rel: string;
          target: string;
          type: string;
        }>;
        abbr: ElementDefinitionWithChildren;
        address: ElementDefinitionWithChildren;
        area: ElementDefinition<{
          alt: string;
          coords: string;
          download: string;
          href: string;
          ping: string;
          referrerpolicy: string;
          rel: string;
          shape: string;
          target: string;
        }>;
        article: ElementDefinitionWithChildren;
        aside: ElementDefinitionWithChildren;
        audio: ElementDefinitionWithChildren<{
          autoplay: string;
          controls: string;
          controlslist: string;
          crossorigin: string;
          anonymous: string;
          useCredentials: boolean;
          disableremoteplayback: string;
          loop: string;
          muted: string;
          preload: string;
          src: string;
        }>;
        b: ElementDefinitionWithChildren;
        base: ElementDefinition<{ href: string; target: string }>;
        bdi: ElementDefinitionWithChildren;
        bdo: ElementDefinitionWithChildren;
        blockquote: ElementDefinitionWithChildren<{ cite: string }>;
        body: ElementDefinitionWithChildren<{
          onafterprint: string;
          onbeforeprint: string;
          onbeforeunload: string;
          onblur: string;
          onerror: string;
          onfocus: string;
          onhashchange: string;
          onlanguagechange: string;
          onload: string;
          onmessage: string;
          onoffline: string;
          ononline: string;
          onpopstate: string;
          onredo: string;
          onresize: string;
          onstorage: string;
          onundo: string;
          onunload: string;
        }>;
        br: ElementDefinition;
        button: ElementDefinitionWithChildren<{
          disabled: boolean;
          formaction: string;
          formenctype: FormEncType;
          formmethod: string;
          formnovalidate: string;
          formtarget: string;
          name: string;
          popovertarget: string;
          popovertargetaction: string;
          hide: string;
          show: string;
          toggle: string;
          type: "button" | "reset" | "submit";
          value: string;
          form: string;
        }>;
        canvas: ElementDefinitionWithChildren<{
          height: string;
          width: string;
        }>;
        caption: ElementDefinitionWithChildren;
        cite: ElementDefinitionWithChildren;
        code: ElementDefinitionWithChildren;
        col: ElementDefinitionWithChildren<{ span: number }>;
        colgroup: ElementDefinitionWithChildren<{ span: number }>;
        data: ElementDefinitionWithChildren<{ value: string }>;
        datalist: ElementDefinitionWithChildren;
        dd: ElementDefinitionWithChildren;
        del: ElementDefinitionWithChildren<{ cite: string; datetime: string }>;
        details: ElementDefinitionWithChildren<{ open: boolean }>;
        dfn: ElementDefinitionWithChildren;
        dialog: ElementDefinitionWithChildren<{ open: boolean }>;
        div: ElementDefinitionWithChildren;
        dl: ElementDefinitionWithChildren;
        dt: ElementDefinitionWithChildren;
        em: ElementDefinitionWithChildren;
        embed: ElementDefinitionWithChildren<{
          height: string;
          src: string;
          type: string;
          width: string;
        }>;
        fieldset: ElementDefinitionWithChildren<{
          disabled: boolean;
          name: string;
          form: string;
        }>;
        figcaption: ElementDefinitionWithChildren;
        figure: ElementDefinitionWithChildren;
        footer: ElementDefinitionWithChildren;
        form: ElementDefinitionWithChildren<{
          acceptCharset: string;
          autocapitalize: string;
          autocomplete: string;
          enctype: string;
          name: string;
          rel: string;
          action: string;
          method: string;
          novalidate: string;
          target: string;
        }>;
        h1: ElementDefinitionWithChildren;
        h2: ElementDefinitionWithChildren;
        h3: ElementDefinitionWithChildren;
        h4: ElementDefinitionWithChildren;
        h5: ElementDefinitionWithChildren;
        h6: ElementDefinitionWithChildren;
        head: ElementDefinitionWithChildren;
        header: ElementDefinitionWithChildren;
        hgroup: ElementDefinitionWithChildren;
        hr: ElementDefinition;
        html: ElementDefinitionWithChildren;
        i: ElementDefinitionWithChildren;
        iframe: ElementDefinitionWithChildren<{
          allow: string;
          allowfullscreen: string;
          referrerpolicy: string;
          height: string;
          loading: string;
          name: string;
          sandbox: string;
          src: string;
          srcdoc: string;
          width: string;
        }>;
        img: ElementDefinition<{
          alt: string;
          crossorigin: string;
          anonymous: string;
          decoding: string;
          sync: string;
          async: string;
          elementtiming: string;
          fetchpriority: string;
          high: string;
          low: string;
          auto: string;
          height: string;
          ismap: string;
          loading: string;
          eager: string;
          lazy: string;
          sizes: string;
          src: string;
          srcset: string;
          width: string;
          usemap: string;
          referrerpolicy: ReferrerPolicy;
          useCredentials: string;
        }>;
        input: ElementDefinition<{
          accept: string;
          alt: string;
          autocapitalize: string;
          autocomplete: string;
          capture: string;
          checked: boolean;
          dirname: string;
          disabled: boolean;
          formaction: string;
          formenctype: FormEncType;
          formmethod: FormMethod;
          formnovalidate: string;
          formtarget: string;
          height: string;
          list: string;
          max: string;
          maxlength: string;
          min: string;
          minlength: string;
          multiple: string;
          name: string;
          pattern: string;
          placeholder: string;
          popovertarget: string;
          popovertargetaction: string;
          readonly: string;
          required: boolean;
          size: string;
          src: string;
          step: string;
          type:
            | "button"
            | "checkbox"
            | "color"
            | "date"
            | "datetime-local"
            | "email"
            | "file"
            | "hidden"
            | "image"
            | "month"
            | "number"
            | "password"
            | "radio"
            | "range"
            | "reset"
            | "search"
            | "submit"
            | "tel"
            | "text"
            | "time"
            | "url"
            | "week";
          value: string;
          width: string;
          form: string;
        }>;
        ins: ElementDefinitionWithChildren;
        kbd: ElementDefinitionWithChildren;
        label: ElementDefinitionWithChildren<{ for: string }>;
        legend: ElementDefinitionWithChildren;
        li: ElementDefinitionWithChildren<{ value: string | number }>;
        link: ElementDefinition<{
          as: string;
          crossorigin: "anonymous" | "use-credentials";
          disabled: boolean;
          fetchpriority: "high" | "low" | "auto";
          href: string;
          hreflang: string;
          imagesizes: string;
          imagesrcset: string;
          integrity: string;
          media: string;
          referrerpolicy: ReferrerPolicy;
          rel: string;
          sizes: string;
          title: string;
          type: string;
        }>;
        main: ElementDefinitionWithChildren;
        map: ElementDefinitionWithChildren<{ name: string }>;
        mark: ElementDefinitionWithChildren;
        menu: ElementDefinitionWithChildren;
        meta: ElementDefinitionWithChildren<{
          content: string;
          httpEquiv: string;
          name: string;
          charset: string;
        }>;
        meter: ElementDefinitionWithChildren<{
          value: string;
          min: string;
          max: string;
          low: string;
          high: string;
          optimum: string;
          form: string;
        }>;
        nav: ElementDefinitionWithChildren;
        noscript: ElementDefinition;
        object: ElementDefinitionWithChildren<{
          data: string;
          height: string;
          name: string;
          type: string;
          usemap: string;
          width: string;
          form: string;
        }>;
        ol: ElementDefinitionWithChildren<{
          reversed: string | boolean;
          start: string | number;
          type: string;
        }>;
        optgroup: ElementDefinitionWithChildren<{
          disabled: boolean;
          label: string;
        }>;
        option: ElementDefinition<{
          disabled: boolean;
          selected: boolean;
          label: string;
          value: string;
          children: string;
        }>;
        output: ElementDefinitionWithChildren<{
          for: string;
          name: string;
          form: string;
        }>;
        p: ElementDefinitionWithChildren;
        picture: ElementDefinitionWithChildren;
        pre: ElementDefinitionWithChildren;
        progress: ElementDefinitionWithChildren<{ max: string; value: string }>;
        q: ElementDefinitionWithChildren<{ cite: string }>;
        rp: ElementDefinitionWithChildren;
        rt: ElementDefinitionWithChildren;
        ruby: ElementDefinitionWithChildren;
        s: ElementDefinitionWithChildren;
        samp: ElementDefinitionWithChildren;
        script: ElementDefinitionWithChildren<{
          async: boolean;
          crossorigin: string;
          defer: boolean;
          fetchpriority: "high" | "low" | "auto";
          integrity: string;
          nomodule: boolean;
          nonce: string;
          referrerpolicy: string;
          src: string;
          type: "importmap" | "module";
        }>;
        search: ElementDefinitionWithChildren;
        section: ElementDefinitionWithChildren;
        select: ElementDefinitionWithChildren<{
          autocomplete: string;
          disabled: boolean;
          multiple: string;
          name: string;
          required: boolean;
          size: string;
          form: string;
        }>;
        slot: ElementDefinitionWithChildren<{ name: string }>;
        small: ElementDefinitionWithChildren;
        source: ElementDefinitionWithChildren<{
          type: string;
          src: string;
          srcset: string;
          sizes: string;
          media: string;
          height: string;
          width: string;
        }>;
        span: ElementDefinitionWithChildren;
        strong: ElementDefinitionWithChildren;
        style: ElementDefinitionWithChildren<{
          media: string;
          nonce: string;
          title: string;
        }>;
        sub: ElementDefinitionWithChildren;
        summary: ElementDefinitionWithChildren;
        sup: ElementDefinitionWithChildren;
        table: ElementDefinitionWithChildren;
        tbody: ElementDefinitionWithChildren;
        td: ElementDefinitionWithChildren<{
          colspan: string;
          headers: string;
          rowspan: string;
        }>;
        template: ElementDefinitionWithChildren<{ shadowrootmode: string }>;
        textarea: ElementDefinitionWithChildren<{
          autocapitalize: string;
          autocomplete: string;
          cols: string;
          dirname: string;
          disabled: boolean;
          maxlength: string;
          minlength: string;
          name: string;
          placeholder: string;
          readonly: string;
          required: boolean;
          rows: string;
          spellcheck: string;
          wrap: string;
          form: string;
        }>;
        tfoot: ElementDefinitionWithChildren;
        th: ElementDefinitionWithChildren<{
          abbr: string;
          colspan: string;
          headers: string;
          rowspan: string;
          scope: string;
        }>;
        thead: ElementDefinitionWithChildren;
        time: ElementDefinitionWithChildren<{ datetime: string }>;
        title: ElementDefinitionWithChildren;
        tr: ElementDefinitionWithChildren;
        track: ElementDefinitionWithChildren<{
          default: string;
          kind: string;
          label: string;
          src: string;
          srclang: string;
        }>;
        u: ElementDefinitionWithChildren;
        ul: ElementDefinitionWithChildren;
        var: ElementDefinitionWithChildren;
        video: ElementDefinitionWithChildren<{
          autoplay: string;
          controls: string;
          controlslist: string;
          crossorigin: string;
          disablepictureinpicture: string;
          disableremoteplayback: string;
          height: string;
          loop: string;
          muted: string;
          playsinline: string;
          poster: string;
          preload: string;
          src: string;
          width: string;
        }>;
        wbr: ElementDefinitionWithChildren;
      }
    }

    namespace SVG {
      type AnimateAttributes = {
        begin: string;
        dur: string;
        end: string;
        min: string;
        max: string;
        restart: string;
        repeatCount: string;
        repeatDur: string;
        fill: string;
        calcMode: string;
        values: string;
        keyTimes: string;
        keySplines: string;
        from: string;
        to: string;
        by: string;
        attributeName: string;
        additive: "replace" | "sum";
        accumulate: "none" | "sum";
      };

      type BlendMode =
        | "normal"
        | "multiply"
        | "screen"
        | "overlay"
        | "darken"
        | "lighten"
        | "color-dodge"
        | "color-burn"
        | "hard-light"
        | "soft-light"
        | "difference"
        | "exclusion"
        | "hue"
        | "saturation"
        | "color"
        | "luminosity";

      type SvgAspectRatioScale =
        | "none"
        | "xMinYMin"
        | "xMidYMin"
        | "xMinYMid"
        | "xMidYMid"
        | "xMinYMax"
        | "xMidYMax"
        | "xMaxYMax";

      type SvgAspectRatioKind = "meet" | "slice";

      type SvgAspectRatio = `${SvgAspectRatioScale} ${SvgAspectRatioKind}`;

      interface ElementTagNameMap {
        animate: ElementDefinition<AnimateAttributes>;
        animateMotion: ElementDefinition<
          AnimateAttributes & {
            keyPoints: number;
            path: string;
            rotate: number;
          }
        >;
        animateTransform: ElementDefinition<
          AnimateAttributes & {
            type: "translate" | "scale" | "rotate" | "skewX" | "skewY";
          }
        >;
        circle: ElementDefinition<{
          cx: number | string;
          cy: number | string;
          r: number | string;
          pathLength: number;
        }>;
        clipPath: ElementDefinitionWithChildren<{
          clipPathUnits: "userSpaceOnUse" | "objectBoundingBox";
        }>;
        defs: ElementDefinitionWithChildren;
        desc: ElementDefinitionWithChildren;
        ellipse: ElementDefinition<{
          cx: number | string;
          cy: number | string;
          rx: number | string;
          ry: number | string;
          pathLength: number;
        }>;
        feBlend: ElementDefinition<{
          in: string;
          in2: string;
          mode: BlendMode;
        }>;
        feColorMatrix: ElementDefinition<{
          in:
            | "SourceGraphic"
            | "SourceAlpha"
            | "BackgroundImage"
            | "BackgroundAlpha"
            | "FillPaint";
          type: "matrix" | "saturate" | "hueRotate" | "luminanceToAlpha";
        }>;
        feComponentTransfer: ElementDefinition<{ in: string }>;
        feComposite: ElementDefinition<{
          in: string;
          in2: string;
          operator:
            | "over"
            | "in"
            | "out"
            | "atop"
            | "xor"
            | "lighter"
            | "arithmentic";
          k1: string;
          k2: string;
          k3: string;
          k4: string;
        }>;
        feConvolveMatrix: ElementDefinition<{
          in: string;
          order: string;
          kernelMatrix: string;
          divisor: string;
          bias: string;
          targetX: string;
          targetY: string;
          edgeMode: string;
          kernelUnitLength: string;
          preserveAlpha: string;
        }>;
        feDiffuseLighting: ElementDefinition<{
          in: string;
          surfaceScale: string;
          diffuseConstant: string;
          kernelUnitLength: string;
        }>;
        feDisplacementMap: ElementDefinition<{
          in: string;
          in2: string;
          scale: string;
          xChannelSelector: string;
          yChannelSelector: string;
        }>;
        feDistantLight: ElementDefinition<{
          azimuth: string;
          elevation: string;
        }>;
        feDropShadow: ElementDefinition<{
          dx: string;
          dy: string;
          stdDeviation: string;
        }>;
        feFlood: ElementDefinition<{
          floodColor: string;
          floodOpacity: string;
        }>;
        feFuncA: ElementDefinition;
        feFuncB: ElementDefinition;
        feFuncG: ElementDefinition;
        feFuncR: ElementDefinition;
        feGaussianBlur: ElementDefinition;
        feImage: ElementDefinition;
        feMerge: ElementDefinition;
        feMergeNode: ElementDefinition;
        feMorphology: ElementDefinition;
        feOffset: ElementDefinition;
        fePointLight: ElementDefinition;
        feSpecularLighting: ElementDefinition;
        feSpotLight: ElementDefinition;
        feTile: ElementDefinition;
        feTurbulence: ElementDefinition;
        filter: ElementDefinition;
        Deprecated: ElementDefinition;
        foreignObject: ElementDefinition;
        g: ElementDefinition;
        image: ElementDefinition;
        line: ElementDefinition;
        linearGradient: ElementDefinition;
        marker: ElementDefinition;
        mask: ElementDefinition;
        metadata: ElementDefinition;
        mpath: ElementDefinition;
        path: ElementDefinition;
        pattern: ElementDefinition;
        polygon: ElementDefinition;
        polyline: ElementDefinition;
        radialGradient: ElementDefinition;
        rect: ElementDefinition;
        // script: ElementDefinition; TODO: enable ASAP
        set: ElementDefinition;
        stop: ElementDefinition;
        // style: ElementDefinition; TODO: enable ASAP
        svg: ElementDefinitionWithChildren<{
          preserveAspectRatio: SvgAspectRatio;
          height: number | string;
          width: number | string;
          viewBox: string;
          x: number | string;
          y: number | string;
        }>;
        switch: ElementDefinition;
        symbol: ElementDefinition;
        text: ElementDefinition;
        textPath: ElementDefinition;
        // title: ElementDefinition; TODO: enable ASAP
        tspan: ElementDefinition;
        use: ElementDefinition;
        view: ElementDefinition;
      }
    }

    interface IntrinsicAttributes {}
    interface Element extends Hast.Element {}

    type Node =
      | JSX.Element
      | JSX.Node[]
      | string
      | number
      | boolean
      | undefined;

    interface ElementChildrenAttribute {
      children: {};
    }

    interface IntrinsicElements
      extends Override<
          {
            [K in keyof HTMLElementTagNameMap]: JSX.Attributes<
              HTMLElementTagNameMap[K]
            >;
          },
          JSX.HTML.ElementTagNameMap
        >,
        Override<
          {
            [K in keyof Omit<
              SVGElementTagNameMap,
              "a" | "script" | "style" | "title" // TODO: find a nice way to express the difference between a tags in HTML and SVG
            >]: JSX.Attributes<SVGElementTagNameMap[K]>;
          },
          JSX.SVG.ElementTagNameMap
        > {}
  }
}
