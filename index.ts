/// <reference lib="dom" />
import "./jsx.js";
import * as HastToHtml from "hast-util-to-html";
import * as HastFromHtml from "hast-util-from-html";
import { h } from "hastscript";
import { visit } from "unist-util-visit";

const isObject = (x: unknown): x is Record<string | number | symbol, unknown> =>
  typeof x === "object" && x !== null;

const isElement = (x: unknown): x is Element =>
  isObject(x) && "type" in x && x.type === "element";

const toKebapCase = (string: string) =>
  string.replaceAll(/([A-Z])/g, (match) => `-${match.toLowerCase()}`);

const isJsx = (x: unknown): x is JSX.Element | JSX.Element[] =>
  (Array.isArray(x) && x.every(isJsx)) ||
  (isObject(x) && "__jsx" in x && x.__jsx === jsxBrand);

const elementToHtml = (element: JSX.Element) => {
  return HastToHtml.toHtml(element);
};

const transformProperties = (properties: JSX.Element["properties"]) =>
  propertiesTransformers.reduce(
    (props, transformer) => transformer(props),
    properties,
  );

const jsxBrand = Symbol("jsx");

const toBranded = <T extends Record<string, any>>(x: T) => ({
  ...x,
  __jsx: jsxBrand,
});

export function Fragment({ children }: { children: any }): JSX.Element {
  return typeof children === "string"
    ? { type: "text", value: children }
    : Array.isArray(children)
      ? children.reduce<JSX.Element[]>((acc, child) => {
          if (child == null) {
            return acc;
          }
          acc.push(Fragment({ children: child }));
          return acc;
        }, [])
      : children;
}

export function createElement(
  tag: string | Function,
  { children, ...attributes }: Record<string, any>,
): JSX.Element {
  const result =
    typeof tag === "string"
      ? h(tag, transformProperties(attributes), children)
      : tag({ children, ...attributes });

  return !Array.isArray(result)
    ? toBranded(result)
    : result.flat().map(toBranded);
}

export const toHtml = (element: JSX.Element | JSX.Element[]) => {
  return Array.isArray(element)
    ? element.map(elementToHtml).join("")
    : elementToHtml(element);
};

export const fromHtml = (unsafeHtml: string) => {
  const node = HastFromHtml.fromHtml(unsafeHtml, {
    fragment: true,
  });
  visit(node, (node) => {
    delete node.position;
  });

  const [head, ...tail] = node.children.reduce((children, child) => {
    if (!isElement(child)) {
      return children;
    }
    children.push(toBranded(child) as JSX.Element);
    return children;
  }, [] as JSX.Element[]);

  return tail.length === 0 ? head : [head, ...tail];
};

export * from "hast-util-select";

/**
 * you can "post-process" properties by adding a function
 * to this array.
 *
 * @example
 *
 * ```ts
 * propertiesTransformers.push(function stringifyClass(properties) {
 *   // check if this function should transform anything first
 *   if ("class" in properties) {
 *     properties.className = !isObject(properties.class)
 *       ? properties.class // no need to transform
 *       : Object.entries(properties.class) // transform object into class string, similar to reacts class utils
 *           .filter(([, enabled]) => enabled)
 *           .map(([key]) => key)
 *           .join(" ");
 *     delete properties.class; // clean up, "class" does not exist as an idl attribute
 *   }
 *   return properties;
 * });
 * ```
 */
export const propertiesTransformers: Array<
  (properties: JSX.Element["properties"]) => JSX.Element["properties"]
> = [];

propertiesTransformers.push(function stringifyClass(properties) {
  if ("class" in properties) {
    properties.className = !isObject(properties.class)
      ? properties.class
      : Object.entries(properties.class)
          .filter(([, enabled]) => enabled)
          .map(([key]) => key)
          .join(" ");
    delete properties.class;
  }
  return properties;
});

propertiesTransformers.push(function stringifyStyle(properties) {
  if ("style" in properties && isObject(properties.style)) {
    properties.style = Object.entries(properties.style)
      .map(([key, value]) => `${toKebapCase(key)}:${value}`)
      .join(";");
  }
  return properties;
});
