import { describe, test } from "node:test";
import assert from "node:assert";
import { fromHtml, toHtml } from "./index.js";

describe("components", () => {
  test("plain component", () => {
    function Foo() {
      return <div></div>;
    }
    assert.equal(toHtml(<Foo />), "<div></div>");
  });
});

describe("passing attributes", () => {
  test("should only be applied where the component says", () => {
    function Foo(props: JSX.Attributes) {
      return (
        <div>
          <input {...props} />
        </div>
      );
    }
    assert.equal(
      toHtml(<Foo hx-get="foo" />),
      `<div><input hx-get="foo"></div>`,
    );
  });

  test("style", () => {
    assert.equal(
      toHtml(<div style={{ color: "red", padding: "2rem" }}></div>),
      `<div style="color:red;padding:2rem"></div>`,
    );
  });
});

describe("class expansion", () => {
  test("mixed classes", () => {
    assert.equal(
      toHtml(<div class={{ foo: true, bar: false }}></div>),
      `<div class="foo"></div>`,
    );
  });
});

describe("fragments", () => {
  test("plain text as fragment child", () => {
    assert.doesNotThrow(() => {
      toHtml(
        <>
          <div>test</div>
          test
        </>,
      );
    });
  });
});

describe("styles", () => {
  test("margin", () => {
    assert(
      toHtml(<div style={{ marginRight: "3px" }}>test</div>) ===
        '<div style="margin-right:3px">test</div>',
    );
  });
});

describe("fromHtml", () => {
  test("root", () => {
    assert.deepEqual(
      fromHtml("<div>Hello world</div>"),
      <div>Hello world</div>,
    );
  });

  test("embedded", () => {
    assert.deepEqual(
      <div>{fromHtml("<span>foo</span>")}</div>,
      <div>
        <span>foo</span>
      </div>,
    );
  });
});

describe("custom elements", () => {
  test("differentiating between node and props", () => {
    // @ts-ignore - foo-bar can't be added to the global elements without polluting downstream consumers
    const x = <foo-bar type="date" value=""></foo-bar>;
    assert.equal(toHtml(x), `<foo-bar type="date" value=""></foo-bar>`);
  });
});
