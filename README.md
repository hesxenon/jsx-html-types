# tsx-to-html

A simple jsx runtime to transform your tsx into html strings.

## Features

- small code base
- sensible expansions to HTML that are possible with jsx (style declarations, etc.)
- represents HTML as closely as possible
  - no `className` hacks
  - no extraneous properties that HTML actually doesn't support
- utilizes the [hast ecosystem](https://unifiedjs.com/explore/topic/hast/)

## Usage

```jsonc
// tsconfig.json
{
  "jsx": "react-jsx",
  "jsxImportSource": "tsx-to-html"
}
```

```typescript
import { toHtml } from "tsx-to-html"

toHtml(<div class="foo"></div>) // '<div class="foo"></div>'
```

## Transformers

You can influence how jsx elements (hast elements) are being transformed into strings
by pushing a properties transformer (`Properties -> Properties`) into the
`propertiesTransformers` array.

In fact that's what this library is doing to transform class and style objects to
strings:

```typescript
import * as TSX from "tsx-to-html";

TSX.propertiesTransformers.push((properties) => {
  if ("class" in properties && isObject(properties["class"])) {
    properties["class"] = Object.entries(properties["class"])
      .filter(([, enabled]) => enabled)
      .map(([key]) => key)
      .join(" ");
  }
  return properties;
});
```

## Differences to regular HTML

### Class expansion

You can use a `Record<string, boolean>` to define which classes will be included
in the output

```typescript
toHtml(<div class={{foo: true, bar: false}}></div>) // '<div class="foo"></div>'
```

### Style declarations

Inline styles can be given via a `CSSStyleDeclaration` object, similar to react.

```typescript
toHtml(<div style={{display: "flex", flexDirection: "column"}}></div>) // '<div style="display:flex;flex-direction:column"></div>'`
```

## Why another jsx transformation lib?

Every other lib I've seen writes a lot of pre-existing logic themselves. Why not just re-use libraries like hastscript who have millions of downloads per week and will probably do their job better and faster?
